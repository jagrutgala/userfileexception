﻿using System;

namespace UserFileException.Exceptions {
    internal class UserAlreadyExistsError : Exception {
        public UserAlreadyExistsError() : base("User Already Exists ![Error]"){

        }
    }
}
