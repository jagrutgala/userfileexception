﻿using System;
using UserFileException.Models;
using UserFileException.Exceptions;

namespace UserFileException.Repository {
    internal class UserRepository {
        string userFilename;
        List<User> users;

        public UserRepository() {
            this.userFilename = "users.txt";
            users = new List<User>();
        }

        public void RegisterUser(User u) {
            bool isUserOk = false;
            try {
                isUserOk = this.CheckUserList(u.name);
            }
            catch (UserAlreadyExistsError er) {
                Console.WriteLine(er.Message);
            }
            if (isUserOk) {
                Console.WriteLine("User Added");
                this.users.Add(u);
                this.WriteUserList();
            }
        }

        public bool CheckUserList(string username) {
            using (StreamReader sr = new StreamReader(this.userFilename)) {
                string line;
                while ((line = sr.ReadLine()) != null) {
                    string[] u = line.Split(',');
                    this.users.Add(new User(u[0], u[1]));
                    if (line.Split(',')[0] == username) {
                        throw new UserAlreadyExistsError();
                    }
                }
            }
            return true;
        }

        public void WriteUserList() {
            using (StreamWriter sw = new StreamWriter(this.userFilename)) {
                foreach (var u in this.users) {
                    sw.WriteLine($"{u.ToString()}");
                }
            }
        }

    }
}
