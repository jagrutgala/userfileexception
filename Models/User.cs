﻿using System;

namespace UserFileException.Models {
    internal class User {
        public string name;
        public string city;

        public User(string name, string city) {
            this.name = name;
            this.city = city;
        }

        public override string ToString() {
            return $"{this.name},{this.city}";
        }
    }
}
